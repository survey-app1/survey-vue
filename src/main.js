import Vue from 'vue'
import App from './App.vue'
// import 'bootstrap'; 
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import router from './routes'
import BootstrapVue from 'bootstrap-vue'
import store from "./store";
import api from "./api";
import VueI18n from 'vue-i18n'
const enDictionary = require('./assets/en.static.json')
const ruDictionary = require('./assets/ru.static.json')

Vue.use(BootstrapVue)
Vue.use(VueI18n)

Vue.config.productionTip = false

api.getSubmissions().then(response => {
  store.self.commit("updateSubmissions", response.data);
}) 

const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages: {
  }
});

api.getQuestions().then(response => {
  const questions = response.data.map(qItem => {
    return {
      qId: qItem.id,
      qText: qItem.text,
      qLanguage: qItem.language,
      answers: qItem.availableAnswers.map(aItem => {
        return {
          aId: aItem.id,
          aText: aItem.text
        };
      })
    };

  });
  let qEnTranslations = {}
  let qRuTranslations = {}
  response.data.forEach(qItem => {

    if (qItem.language === 'ru') {
      qRuTranslations[qItem.id + '_text'] = qItem.text
      qRuTranslations[qItem.id + '_title'] = qItem.title
    }
    else {
      qEnTranslations[qItem.id + '_text'] = qItem.text
      qEnTranslations[qItem.id + '_title'] = qItem.title
  }})
  i18n.setLocaleMessage('en', { ...enDictionary, ...qEnTranslations })
  i18n.setLocaleMessage('ru', { ...ruDictionary, ...qRuTranslations })
  store.self.commit("loadQuestions", questions);
  store.self.commit("changeLanguage", 'en');
}).then(() => {
  new Vue({
    render: h => h(App),
    router,
    i18n
  }).$mount('#app')
})