import axios from 'axios'

// const baseURL = `http://localhost:8085/`
const baseURL = `http://194.67.93.97:8085/`

const AXIOS = axios.create({
  baseURL: baseURL,
  timeout: 1000,
  headers: {'content-type': 'application/json'}
});


export default {
    submitSurvey(payload) {
        return AXIOS.post('/survey' , payload);
    },
    getSubmissions() {
      return AXIOS.get('/submissions');
    },
    getQuestions() {
      return AXIOS.get('/questions');
    },
    baseUrl() {
      console.log(baseURL)
      return baseURL
    }
}