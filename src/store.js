import Vuex from 'vuex'
import Vue from 'vue'
import api from './api.js'

Vue.use(Vuex)

const self = new Vuex.Store({
  state: {
    language: "",
    answers: {},
    questions: [],
    personnelNumber: "",
    submissions: [],
    maximumScore: 0
  },
  mutations: {
    changeAnswer(state, answered) {
      state.answers[answered.qId] = answered.aId
    },
    changePersonnelNumber(state, personnelNumber) {
      state.personnelNumber = personnelNumber
    },
    updateSubmissions(state, submissions) {
      state.submissions = submissions;
    },
    loadQuestions(state, questions) {
      state.questions = questions;
      state.maximumScore = questions.length;
    },
    clearData(state) {
      state.answers = {};
    },
    changeLanguage(state, language) {
      state.language = language;
    }
  },
  getters: {
    getQuestionIdsForCurrentLanguage: state => {
      return state.questions.filter(q => q.qLanguage === state.language).map(q => q.qId);
    }
  },
  actions: {
    loadAllQuestions({ commit }) {
      const questions = api.getQuestions().then(response => {
        return response.data.map(qItem => {
          return {
            qId: qItem.id,
            qText: qItem.text,
            qTitle: qItem.title,
            answers: qItem.availableAnswers.map(aItem => {
              return {
                aId: aItem.id,
                aText: aItem.text
              };
            })
          };
        });

      });
      commit("loadQuestions", questions);
    }
  }
})

export default {
  self
}