import Vue from 'vue'
import VueRouter from 'vue-router'
import Submitted from '../src/components/Submitted.vue'
import AdminPanel from '../src/components/AdminPanel.vue'
import MainForm from '../src/components/MainForm.vue'

Vue.use(VueRouter)

const routes = [
    { path: '/success', component: Submitted },
    { path: '/admin', component: AdminPanel },
    { path: '/submit', component: MainForm },
    { path: '*', redirect: '/submit'}
]




export default new VueRouter({
    routes
});



